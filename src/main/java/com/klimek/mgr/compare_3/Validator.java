package com.klimek.mgr.compare_3;

import static java.lang.Thread.sleep;

public class Validator {
    public static boolean isValidRoleToRegister(Role role) {
        try {
            // it is a equivalent of REST privileges check
            sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return !Role.worker.equals(role);
    }
    public static boolean isValidRoleToGetUpdates(Role role) {
        try {
            // it is a equivalent of REST privileges check
            sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Role.admin.equals(role);
    }

    public enum Role {
        worker, team_leader, admin
    }
}
