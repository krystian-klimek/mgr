package com.klimek.mgr.compare_3.proxy;

import com.klimek.mgr.compare_3.TestUser;
import com.klimek.mgr.compare_3.Validator;

import static com.klimek.mgr.compare_3.Validator.isValidRoleToGetUpdates;
import static com.klimek.mgr.compare_3.Validator.isValidRoleToRegister;

public class Proxy implements Subject {
    private Subject subject = new RealSubject();

    @Override
    public TestUser request(Validator.Role role) {
        if (isValidRoleToRegister(role)) {
            return subject.request(role);
        }
        return null;
    }

    @Override
    public TestUser getData(TestUser user) {
        if (isValidRoleToGetUpdates(user.getRole())) {
            return subject.getData(user);
        }
        return null;
    }

    @Override
    public void close() {
        subject.close();
    }
}
