package com.klimek.mgr.compare_3.proxy;

import com.klimek.mgr.compare_3.TestUser;
import com.klimek.mgr.compare_3.Validator;

public interface Subject {
    TestUser request(Validator.Role role);
    TestUser getData(TestUser id);
    void close();
}
