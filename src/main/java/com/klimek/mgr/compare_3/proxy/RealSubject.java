package com.klimek.mgr.compare_3.proxy;

import com.klimek.mgr.compare_3.TestUser;
import com.klimek.mgr.compare_3.Validator;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;

public class RealSubject extends TimerTask implements Subject {
    private Collection<TestUser> users = new ConcurrentLinkedDeque<>();
    private Timer timer = new Timer();

    public RealSubject() {
        timer.scheduleAtFixedRate(this, 0, 500);
    }

    @Override
    public void run() {
        users.forEach(user -> user.setName("User no: " + System.currentTimeMillis()));
    }

    @Override
    public TestUser request(Validator.Role role) {
        TestUser testUser = new TestUser(role);
        users.add(testUser);
        return testUser;
    }

    @Override
    public TestUser getData(TestUser toGet) {
        return users.stream()
                .filter(user -> toGet.getName().equals(user.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void close() {
        users.clear();
        timer.cancel();
    }
}
