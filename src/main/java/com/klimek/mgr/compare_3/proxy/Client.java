package com.klimek.mgr.compare_3.proxy;

import com.klimek.mgr.compare_3.TestUser;
import com.klimek.mgr.compare_3.Validator;

import java.util.HashSet;
import java.util.Set;

import static com.klimek.mgr.compare_3.TestMethods.NUMBER_OF_HOSTS;
import static com.klimek.mgr.compare_3.TestMethods.NUMBER_OF_USER_LOOPS;
import static java.util.Objects.nonNull;

public class Client {
    public static void main(String[] args) {
        System.out.println("Start Proxy");
        long time = System.currentTimeMillis();
        for (long i = 0; i < NUMBER_OF_HOSTS; i++) {
            Proxy proxy = new Proxy();
            Set<TestUser> users = new HashSet<>();
            for (long j = 0; j < NUMBER_OF_USER_LOOPS; j++) {
                addUserIfIsValid(proxy, users, Validator.Role.worker);
                addUserIfIsValid(proxy, users, Validator.Role.team_leader);
                addUserIfIsValid(proxy, users, Validator.Role.admin);
            }
            users.forEach(proxy::getData);
            proxy.close();
        }
        System.out.println("Time Proxy: " + (System.currentTimeMillis() - time));
    }

    private static void addUserIfIsValid(Proxy proxy, Set<TestUser> users, Validator.Role worker) {
        TestUser user = proxy.request(worker);
        if (nonNull(user)) {
            users.add(user);
        }
    }
}
