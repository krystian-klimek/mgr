package com.klimek.mgr.compare_3.singleton;

import com.klimek.mgr.compare_1.TestMethods;

public class Client {
    public static void main(String[] args) {
        System.out.println("Start Singleton");
        long time = System.currentTimeMillis();
        for (long i = 0; i < TestMethods.NUMBER_OF_LOOPS; i++) {
            Singleton.getInstance().buildTestObjectDTO();
        }
        System.out.println("Time Singleton: " + (System.currentTimeMillis() - time));
    }
}
