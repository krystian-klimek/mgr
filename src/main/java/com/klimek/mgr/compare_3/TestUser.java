package com.klimek.mgr.compare_3;

public class TestUser {
    private String name;
    private Validator.Role role;

    public TestUser(Validator.Role role) {
        this.name = role + "_" + Math.random();
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Validator.Role getRole() {
        return role;
    }
}
