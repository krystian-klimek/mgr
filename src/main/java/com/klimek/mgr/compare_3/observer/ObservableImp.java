package com.klimek.mgr.compare_3.observer;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;

import static com.klimek.mgr.compare_3.Validator.isValidRoleToGetUpdates;
import static com.klimek.mgr.compare_3.Validator.isValidRoleToRegister;

    public class ObservableImp extends TimerTask implements Observable {
    private Collection<Observer> observers = new ConcurrentLinkedDeque<>();
    private Timer timer = new Timer();

    public ObservableImp() {
        timer.scheduleAtFixedRate(this, 0, 500);
    }

    @Override
    public void run() {
        observers
                .stream()
                .filter(observer -> isValidRoleToGetUpdates(observer.getRole()))
                .forEach(observer -> observer.update("User no: " + System.currentTimeMillis()));
    }

    @Override
    public boolean register(Observer observer) {
        boolean validRoleToRegister = isValidRoleToRegister(observer.getRole());
        if (validRoleToRegister) {
            observers.add(observer);
        }

        return validRoleToRegister;
    }

    @Override
    public void close() {
        observers.clear();
        timer.cancel();
    }
}
