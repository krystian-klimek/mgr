package com.klimek.mgr.compare_3.observer;

import com.klimek.mgr.compare_3.Validator;

import static com.klimek.mgr.compare_3.TestMethods.NUMBER_OF_HOSTS;
import static com.klimek.mgr.compare_3.TestMethods.NUMBER_OF_USER_LOOPS;

public class Client {
    public static void main(String[] args) {
        System.out.println("Start Observer");
        long time = System.currentTimeMillis();
        for (long i = 0; i < NUMBER_OF_HOSTS; i++) {
            Observable observable = new ObservableImp();
            for (long j = 0; j < NUMBER_OF_USER_LOOPS; j++) {
                observable.register(new ObserverImp(Validator.Role.worker));
                observable.register(new ObserverImp(Validator.Role.team_leader));
                observable.register(new ObserverImp(Validator.Role.admin));
            }
            observable.close();
        }
        System.out.println("Time Observer: " + (System.currentTimeMillis() - time));
    }
}
