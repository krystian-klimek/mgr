package com.klimek.mgr.compare_3.observer;

import com.klimek.mgr.compare_3.TestUser;
import com.klimek.mgr.compare_3.Validator;

public class ObserverImp implements Observer {
    private TestUser user;

    public ObserverImp(Validator.Role role) {
        this.user = new TestUser(role);
    }

    @Override
    public Validator.Role getRole() {
        return user.getRole();
    }

    @Override
    public void update(String newName) {
        user.setName(newName);
    }
}
