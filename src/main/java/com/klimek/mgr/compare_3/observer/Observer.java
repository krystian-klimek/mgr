package com.klimek.mgr.compare_3.observer;

import com.klimek.mgr.compare_3.Validator;

public interface Observer {
    Validator.Role getRole();
    void update(String newName);
}
