package com.klimek.mgr.compare_3.observer;

public interface Observable {
    boolean register(Observer observer);
    void close();
}
