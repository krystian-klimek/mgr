package com.klimek.mgr.with.decorator2;

abstract class DrinkDecorator extends Drink {
    protected Drink drink;

    public DrinkDecorator(Drink drink) {
        this.drink = drink;
        setName(drink.getName() + "+" + getDrinkDecoratorName());
        setPrice(drink.getPrice() + getAdditionalPrice());
    }

    public void decorateDrink() {
        drink.decorateDrink();
        System.out.println("Cost of [" + getName() + "]: " + getPrice());
    }

    public abstract int getAdditionalPrice();

    public abstract String getDrinkDecoratorName();
}
