package com.klimek.mgr.with.decorator2;

class Coffee extends Drink {
    public Coffee() {
        super();
        setName("Coffee");
        setPrice(10);
    }

    public void decorateDrink() {
        System.out.println("Cost of " + name + ": " + price);
    }
}