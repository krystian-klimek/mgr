package com.klimek.mgr.with.decorator2;

class SugarDecorator extends DrinkDecorator {
    public SugarDecorator(Drink drink) {
        super(drink);
    }

    public void decorateDrink() {
        super.decorateDrink();
        decorateSugar();
    }

    public void decorateSugar() {
        System.out.println("Added " + getDrinkDecoratorName() + " to: " + drink.getName());
    }

    public int getAdditionalPrice() {
        return 5;
    }

    public String getDrinkDecoratorName() {
        return "Sugar";
    }
}
