package com.klimek.mgr.with.decorator2;

public abstract class Drink {
    protected String name;
    protected int price;

    public Drink() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected void setPrice(int price) {
        this.price = price;
    }

    protected int getPrice() {
        return price;
    }

    protected abstract void decorateDrink();
}
