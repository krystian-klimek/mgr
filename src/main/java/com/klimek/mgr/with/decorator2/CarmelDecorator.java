package com.klimek.mgr.with.decorator2;

class CarmelDecorator extends DrinkDecorator {
    public CarmelDecorator(Drink drink) {
        super(drink);
    }

    public void decorateDrink() {
        super.decorateDrink();
        decorateCarmel();
    }

    public void decorateCarmel() {
        System.out.println("Added " + getDrinkDecoratorName() + " to: " + drink.getName());
    }

    public int getAdditionalPrice() {
        return 6;
    }

    public String getDrinkDecoratorName() {
        return "Carmel";
    }
}