package com.klimek.mgr.with.decorator2;

class Tea extends Drink {
    public Tea() {
        super();
        setName("Tea");
        setPrice(5);
    }

    public void decorateDrink() {
        System.out.println("Cost of " + name + ": " + price);
    }
}
