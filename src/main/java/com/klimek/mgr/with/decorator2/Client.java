package com.klimek.mgr.with.decorator2;

import static com.klimek.mgr.TestMethods.NUMBER_OF_USER_LOOPS;

public class Client {
    public static void main(String[] args){
        long start = System.currentTimeMillis();

        for (int i=0; i < NUMBER_OF_USER_LOOPS; i++) {
            System.out.println("Super sweet Tea");
            Drink drink = new SugarDecorator(new SugarDecorator(new Tea()));
            drink.decorateDrink();

            System.out.println();

            System.out.println("Double Carmel Coffee");
            drink = new SugarDecorator(new CarmelDecorator(new CarmelDecorator(new Coffee())));
            drink.decorateDrink();
        }

        System.out.println(System.currentTimeMillis() - start);
    }
}
