package com.klimek.mgr.with.factory;

public class ConcreteProductB implements Product {
    public void someMethod() {
        System.out.println("someMethod invoked from ConcreteProductB");
    }
}
