package com.klimek.mgr.with.factory;

public class Factory {

    private ConcreteProductA productA = new ConcreteProductA();
    private ConcreteProductB productB = new ConcreteProductB();
    
    Product createProduct(ConcreteProductType type) throws ClassNotFoundException {
        switch (type) {
            case A:
                return productA;
            case B:
                return productB;
            default:
                throw new ClassNotFoundException("Not implemented yet.");
        }
    }
}
