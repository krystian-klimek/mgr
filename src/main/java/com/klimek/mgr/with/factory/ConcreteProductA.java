package com.klimek.mgr.with.factory;

public class ConcreteProductA implements Product {
    public void someMethod() {
        System.out.println("someMethod invoked from ConcreteProductA");
    }
}
