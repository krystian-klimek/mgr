package com.klimek.mgr.with.factory;

public class Client {
    public static void main(String[] args) throws ClassNotFoundException {
         Factory factory = new Factory();

         invokeMethodFromProduct(factory, ConcreteProductType.A);
         invokeMethodFromProduct(factory, ConcreteProductType.B);
    }

    private static void invokeMethodFromProduct(Factory factory, ConcreteProductType type) throws ClassNotFoundException {
        Product product = factory.createProduct(type);
        product.someMethod();
    }
}
