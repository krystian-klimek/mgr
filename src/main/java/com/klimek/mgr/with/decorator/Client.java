package com.klimek.mgr.with.decorator;

public class Client {
    public static void main(String[] args) {
        Component component = new ConcreteComponentA();
        component.action();

        System.out.println(" # adding new decorator2 to existing component #");

        Component decoratorA1 = new ConcreteDecoratorA(component);
        decoratorA1.action();

        System.out.println(" # adding more new decorators to existing component with decorator2 #");

        Component decoratorA2 = new ConcreteDecoratorA(decoratorA1);
        Component decoratorB = new ConcreteDecoratorB(decoratorA2);
        decoratorB.action();
    }
}
