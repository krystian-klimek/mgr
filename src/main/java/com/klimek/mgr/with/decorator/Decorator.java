package com.klimek.mgr.with.decorator;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public Component getComponent() {
        return component;
    }
}
