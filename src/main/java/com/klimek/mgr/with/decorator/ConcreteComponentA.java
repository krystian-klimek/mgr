package com.klimek.mgr.with.decorator;

public class ConcreteComponentA implements Component {

    public void action() {
        System.out.println("Tea action invoked");
    }
}

