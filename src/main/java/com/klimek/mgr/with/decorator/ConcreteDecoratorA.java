package com.klimek.mgr.with.decorator;

public class ConcreteDecoratorA extends Decorator {
    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    public void action() {
        getComponent().action();
        System.out.println("Sugar action invoked");
    }
}
