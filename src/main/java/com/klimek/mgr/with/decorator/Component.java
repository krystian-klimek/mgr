package com.klimek.mgr.with.decorator;

public interface Component {
    void action();
}
