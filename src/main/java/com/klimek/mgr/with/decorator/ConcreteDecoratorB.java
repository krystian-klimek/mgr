package com.klimek.mgr.with.decorator;

public class ConcreteDecoratorB extends Decorator {
    public ConcreteDecoratorB(Component component) {
        super(component);
    }

    public void action() {
        getComponent().action();
        System.out.println("ConcreteDecoratorB action invoked");
    }
}
