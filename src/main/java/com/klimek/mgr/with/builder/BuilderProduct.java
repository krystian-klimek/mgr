package com.klimek.mgr.with.builder;

public class BuilderProduct {
    private String paramA;
    private String paramB;
    private String paramC;
    private String paramD;
    private String paramE;

    private BuilderProduct(Builder builder) {
        this.paramA = builder.paramA;
        this.paramB = builder.paramB;
        this.paramC = builder.paramC;
        this.paramD = builder.paramD;
        this.paramE = builder.paramE;
    }

    public String getParamA() {
        return paramA;
    }

    public String getParamB() {
        return paramB;
    }

    public String getParamC() {
        return paramC;
    }

    public String getParamD() {
        return paramD;
    }

    public String getParamE() {
        return paramE;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "BuilderProduct{" +
                "paramA='" + paramA + '\'' +
                ", paramB='" + paramB + '\'' +
                ", paramC='" + paramC + '\'' +
                ", paramD='" + paramD + '\'' +
                ", paramE='" + paramE + '\'' +
                '}';
    }

    public static class Builder {
        private String paramA;
        private String paramB;
        private String paramC;
        private String paramD;
        private String paramE;

        private Builder() {
        }

        public Builder paramA(String paramA) {
            this.paramA = paramA;
            return this;
        }

        public Builder paramB(String paramB) {
            this.paramB = paramB;
            return this;
        }

        public Builder paramC(String paramC) {
            this.paramC = paramC;
            return this;
        }

        public Builder paramD(String paramD) {
            this.paramD = paramD;
            return this;
        }

        public Builder paramE(String paramE) {
            this.paramE = paramE;
            return this;
        }

        public Builder from(BuilderProduct product) {
            paramA = product.getParamA();
            paramB = product.getParamB();
            paramC = product.getParamC();
            paramD = product.getParamD();
            paramE = product.getParamE();
            return this;
        }

        public BuilderProduct build() {
            return new BuilderProduct(this);
        }
    }
}
