package com.klimek.mgr.with.builder;

public class Client {

    public static void main(String[] args) {
        BuilderProduct product = BuilderProduct.builder()
                .paramA("A")
                .paramB("B")
                .paramC("C")
                .paramD("D")
                .paramE("E")
                .build();
        printBuilderObject(product);
        product = BuilderProduct.builder()
                .from(product)
                .paramC("CC")
                .paramD("DD")
                .paramE("EE")
                .build();
        printBuilderObject(product);
    }

    private static void printBuilderObject(BuilderProduct product) {
        System.out.println(product.toString());
    }
}
