package com.klimek.mgr.without.builder;

public class NoBuilderProduct {
    private String paramA;
    private String paramB;
    private String paramC;
    private String paramD;
    private String paramE;

    public NoBuilderProduct(String paramA, String paramB, String paramC, String paramD, String paramE) {
        this.paramA = paramA;
        this.paramB = paramB;
        this.paramC = paramC;
        this.paramD = paramD;
        this.paramE = paramE;
    }

    public String getParamA() {
        return paramA;
    }

    public String getParamB() {
        return paramB;
    }

    public String getParamC() {
        return paramC;
    }

    public String getParamD() {
        return paramD;
    }

    public String getParamE() {
        return paramE;
    }

    @Override
    public String toString() {
        return "BuilderProduct{" +
                "paramA='" + paramA + '\'' +
                ", paramB='" + paramB + '\'' +
                ", paramC='" + paramC + '\'' +
                ", paramD='" + paramD + '\'' +
                ", paramE='" + paramE + '\'' +
                '}';
    }
}
