package com.klimek.mgr.without.builder;

public class Client {

    public static void main(String[] args) {
        NoBuilderProduct product = new NoBuilderProduct("A", "B", "C", "D", "E");
        printNonBuilderObject(product);
        product = new NoBuilderProduct(product.getParamA(), product.getParamB(), "CC", "DD", "EE");
        printNonBuilderObject(product);
    }

    private static void printNonBuilderObject(NoBuilderProduct product) {
        System.out.println(product.toString());
    }
}
