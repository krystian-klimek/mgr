package com.klimek.mgr.without.factory;

public class ConcreteProductA implements Product {
    public void someMethod() {
        System.out.println("someMethod invoked from ConcreteProductA");
    }
}
