package com.klimek.mgr.without.factory;

public class ConcreteProductB implements Product {
    public void someMethod() {
        System.out.println("someMethod invoked from ConcreteProductB");
    }
}
