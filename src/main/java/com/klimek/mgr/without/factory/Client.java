package com.klimek.mgr.without.factory;

public class Client {
    public static void main(String[] args) {
         invokeMethodFromProduct(new ConcreteProductA());
         invokeMethodFromProduct(new ConcreteProductB());
    }

    private static void invokeMethodFromProduct(Product product) {
        product.someMethod();
    }
}
