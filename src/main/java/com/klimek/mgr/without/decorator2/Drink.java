package com.klimek.mgr.without.decorator2;

import java.util.ArrayList;
import java.util.List;

public class Drink {
    private String name;
    private Component component;
    private List<ComponentAdditional> addons = new ArrayList<>();

    Drink(Component component, String name) {
        this.component = component;
        this.name = name;
    }

    public void action() {
        component.action();
        addons.forEach(ComponentAdditional::action);
        int totalPrice = addons.stream().map(ComponentAdditional::getPrice).mapToInt(Integer::intValue).sum() + component.getPrice();
        System.out.println("Total price of " + name + " [Ingredients: " + getFullName() + "] is " + totalPrice);
    }

    private String getFullName() {
        StringBuilder name = new StringBuilder(component.getName());
        addons.forEach(addon -> name.append(", ").append(addon.getName()));
        return name.toString();
    }

    public void addAddon(ComponentAdditional addon) {
        addons.add(addon);
    }
}
