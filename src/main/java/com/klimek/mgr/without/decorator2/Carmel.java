package com.klimek.mgr.without.decorator2;

public class Carmel implements ComponentAdditional {

    public void action() {
        System.out.println(getName() + " added");
    }

    @Override
    public int getPrice() {
        return 6;
    }

    @Override
    public String getName() {
        return "Carmel";
    }
}
