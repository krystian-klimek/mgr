package com.klimek.mgr.without.decorator2;

public class Tea implements Component {

    public void action() {
        System.out.println(getName() + " started");
    }

    @Override
    public int getPrice() {
        return 5;
    }

    @Override
    public String getName() {
        return "Tea";
    }
}

