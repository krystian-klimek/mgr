package com.klimek.mgr.without.decorator2;

import static com.klimek.mgr.TestMethods.NUMBER_OF_USER_LOOPS;

public class Client {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        for (int i=0; i < NUMBER_OF_USER_LOOPS; i++) {
            Sugar sugar = new Sugar();
            Carmel carmel = new Carmel();

            System.out.println("Super sweet Tea");
            Drink drink = new Drink(new Tea(), "Super sweet Tea");
            drink.addAddon(sugar);
            drink.addAddon(sugar);
            drink.action();

            System.out.println();

            System.out.println("Double Carmel Coffee");
            drink = new Drink(new Coffee(), "Double Carmel Coffee");
            drink.addAddon(sugar);
            drink.addAddon(carmel);
            drink.addAddon(carmel);
            drink.action();
        }

        System.out.println(System.currentTimeMillis() - start);
    }
}