package com.klimek.mgr.without.decorator2;

public class Coffee implements Component {

    public void action() {
        System.out.println(getName() + " started");
    }

    @Override
    public int getPrice() {
        return 10;
    }

    @Override
    public String getName() {
        return "Coffee";
    }
}

