package com.klimek.mgr.without.decorator2;

public class Sugar implements ComponentAdditional {

    public void action() {
        System.out.println(getName() + " added");
    }

    @Override
    public int getPrice() {
        return 5;
    }

    @Override
    public String getName() {
        return "Sugar";
    }
}
