package com.klimek.mgr.without.decorator2;

public interface Component {
    void action();

    String getName();

    int getPrice();
}
