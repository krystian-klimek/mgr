package com.klimek.mgr.compare_1;

public interface TestMethods {
    long NUMBER_OF_ITERATIONS = 10000;
    long NUMBER_OF_LOOPS = 1000;

    void buildTestObjectDTO();
}
