package com.klimek.mgr.compare_1.singleton;

import com.klimek.mgr.compare_1.TestMethods;
import com.klimek.mgr.compare_1.TestObject;

import java.util.ArrayList;
import java.util.List;

final class Singleton implements TestMethods {
    private static Singleton instance;
    private Singleton() { }

    static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    @Override
    public void buildTestObjectDTO() {
        List<TestObject> list = new ArrayList<>();
        for (long i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            list.add(new TestObject(i));
        }
        list.removeIf(item -> "0".equals(String.valueOf(item.getMod())));
        list.clear();
    }
}