package com.klimek.mgr.compare_1;

public class TestObject {
    private String name;
    private long position;
    private long mod;

    public TestObject(long i) {
        this.name = "name_" + i;
        this.position = i;
        this.mod = i%10000;
    }

    public String getName() {
        return name;
    }

    public long getPosition() {
        return position;
    }

    public long getMod() {
        return mod;
    }
}
