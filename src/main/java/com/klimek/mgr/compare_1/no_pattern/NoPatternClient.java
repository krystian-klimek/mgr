package com.klimek.mgr.compare_1.no_pattern;

import com.klimek.mgr.compare_1.TestMethods;
import com.klimek.mgr.compare_1.TestObject;

import java.util.ArrayList;
import java.util.List;

import static com.klimek.mgr.compare_1.TestMethods.NUMBER_OF_ITERATIONS;

public class NoPatternClient {
    public static void main(String[] args) {
        System.out.println("Start No Pattern");
        long time = System.currentTimeMillis();
        for (long i = 0; i < TestMethods.NUMBER_OF_LOOPS; i++) {
            List<TestObject> list = new ArrayList<>();
            for (long j = 0; j < NUMBER_OF_ITERATIONS; j++) {
                list.add(new TestObject(j));
            }
            list.removeIf(item -> "0".equals(String.valueOf(item.getMod())));
            list.clear();
        }
        System.out.println("Time No Pattern: " + (System.currentTimeMillis() - time));
    }
}
