package com.klimek.mgr.compare_1.abstract_factory;

import static com.klimek.mgr.compare_1.abstract_factory.ConcreteProductType.INSTANCE;

public class ConcreteFactory1 implements Factory {
    private Product product = new ConcreteProductWithInstance();

    @Override
    public Product createProductWithInstance(ConcreteProductType type) {
        if (INSTANCE.equals(type)) {
            return product;
        }
        return null;
    }
}
