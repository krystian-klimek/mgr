package com.klimek.mgr.compare_1.abstract_factory;

public enum ConcreteProductType {
    INSTANCE, NEW_INSTANCE
}
