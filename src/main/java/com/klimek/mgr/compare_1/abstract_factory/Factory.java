package com.klimek.mgr.compare_1.abstract_factory;

public interface Factory {
    Product createProductWithInstance(ConcreteProductType type);
}
