package com.klimek.mgr.compare_1.abstract_factory;

import com.klimek.mgr.compare_1.TestMethods;

public class Client {
    public static void main(String[] args) {
        ConcreteFactory1 factory = new ConcreteFactory1();

        System.out.println("Start Abstract Factory");
        long time = System.currentTimeMillis();
        for (long i = 0; i < TestMethods.NUMBER_OF_LOOPS; i++) {
            factory.createProductWithInstance(ConcreteProductType.INSTANCE).buildTestObjectDTO();
        }
        System.out.println("Time Abstract Factory: " + (System.currentTimeMillis() - time));
    }
}
