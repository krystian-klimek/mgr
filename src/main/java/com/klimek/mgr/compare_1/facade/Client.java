package com.klimek.mgr.compare_1.facade;

import com.klimek.mgr.compare_1.TestMethods;

public class Client {
    public static void main(String[] args) {
        Facade facade = new Facade();

        System.out.println("Start Facade");
        long time = System.currentTimeMillis();
        for (long i = 0; i < TestMethods.NUMBER_OF_LOOPS; i++) {
            facade.buildTestObjectDTO();
        }
        System.out.println("Time Facade: " + (System.currentTimeMillis() - time));
    }
}
