package com.klimek.mgr.compare_1.facade;

final class Facade {
    private final SubFacade subFacade = new SubFacade();

    void buildTestObjectDTO() {
        subFacade.buildTestObjectDTO();
    }
}