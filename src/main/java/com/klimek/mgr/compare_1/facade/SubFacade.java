package com.klimek.mgr.compare_1.facade;

import com.klimek.mgr.compare_1.TestMethods;
import com.klimek.mgr.compare_1.TestObject;

import java.util.ArrayList;
import java.util.List;

public class SubFacade implements TestMethods {
    @Override
    public void buildTestObjectDTO() {
        List<TestObject> list = new ArrayList<>();
        for (long i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            list.add(new TestObject(i));
        }
        list.removeIf(item -> "0".equals(String.valueOf(item.getMod())));
        list.clear();
    }
}
