package com.klimek.mgr.compare_1.factory;

import com.klimek.mgr.compare_1.TestMethods;

public class Client {
    public static void main(String[] args) throws ClassNotFoundException {
        Factory factory = new Factory();

        System.out.println("Start Factory");
        long time = System.currentTimeMillis();
        for (long i = 0; i < TestMethods.NUMBER_OF_LOOPS; i++) {
            invokeMethodFromProduct(factory, ConcreteProductType.INSTANCE);
        }
        System.out.println("Time Factory: " + (System.currentTimeMillis() - time));
    }

    private static void invokeMethodFromProduct(Factory factory, ConcreteProductType type) throws ClassNotFoundException {
        factory.createProduct(type).buildTestObjectDTO();
    }
}
