package com.klimek.mgr.compare_1.factory;

import com.klimek.mgr.compare_1.TestMethods;

public interface Product extends TestMethods {
    void buildTestObjectDTO();
}
