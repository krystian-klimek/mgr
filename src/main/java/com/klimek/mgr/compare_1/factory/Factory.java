package com.klimek.mgr.compare_1.factory;

public class Factory {
    private final Product product = new ConcreteProductWithInstance();
    
    Product createProduct(ConcreteProductType type) throws ClassNotFoundException {
        if (type == ConcreteProductType.INSTANCE) {
            return product;
        }
        throw new ClassNotFoundException("Not implemented yet.");
    }
}
